import Vue from 'vue'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import axios from 'axios'
import VueAxios from 'vue-axios'

import App from '@/App'
import router from '@/router'

Vue.config.productionTip = false

Vue.use(ElementUI, { locale })
Vue.use(VueAxios, axios)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
