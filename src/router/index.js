import Vue from 'vue'
import Router from 'vue-router'
import RecipesMart from '@/components/RecipesMart'
import RecipeForm from '@/components/RecipeForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: RecipesMart
    },
    {
      path: '/recipe',
      name: 'recipe_new',
      component: RecipeForm
    },
    {
      path: '/recipe/:id',
      name: 'recipe_edit',
      component: RecipeForm,
      props: true
    },
  ]
})
