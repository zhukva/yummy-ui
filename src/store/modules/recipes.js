import axios from 'axios'

export default {
  namespaced: true,
  state: {
    aboutToBeDel: []
  },
  mutations: {
    sync (state) {
      console.log('run sync')
    },
    markAsDel (state, payload) {
      state.aboutToBeDel.push(payload.id)
    }
  },
  actions: {
    sync ({ commit }) {
      commit('sync')
    },
    markAsDel ({ commit }, payload) {
      commit('markAsDel', payload)
    }
  }
}
