import { Model } from '@vuex-orm/core'

export default class Recipe extends Model {
  static entity = 'recipes'

  static fields () {
    return {
      id: this.attr(null),
      title: this.attr(''),
      about: this.attr(''),
      components: this.attr('')
    }
  }
}
