import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import database from '@/store/database'

Vue.use(Vuex)

const store = new Vuex.Store({
  plugins: [VuexORM.install(database)],
  state: {
    favorites: []
  },
  mutations: {
    addFavoriteRecipe (state, payload) {
      if (state.favorites.indexOf(payload.recipe) == -1) {
        state.favorites.push(payload.recipe)
      }
    },
    removeFavoriteRecipe (state, payload) {
      state.favorites.splice(state.favorites.indexOf(payload.recipe), 1)
    }
  },
  actions: {
    addFavoriteRecipe (context, payload) {
      context.commit('addFavoriteRecipe', payload)
    },
    removeFavoriteRecipe (context, payload) {
      context.commit('removeFavoriteRecipe', payload)
    }
  }
})

export default store
