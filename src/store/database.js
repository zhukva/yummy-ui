import { Database } from '@vuex-orm/core'
import Recipe from '@/store/models/Recipe'
import recipes from '@/store/modules/recipes'

const database = new Database()

database.register(Recipe, recipes)

export default database
